<?php
require_once "Controller/MateriaController.php";
require_once "Controller/ProfesorController.php";
require_once "Controller/UsuarioController.php";


define('BASE_URL', '//' . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['PHP_SELF']) . '/');

if (!empty($_GET['action'])) {
    $action = $_GET['action'];
} else {
    $action = 'home';
}

$params = explode('/', $action);

$materiaController = new MateriaController();
$profesorController = new ProfesorController();
$usuarioController = new UsuarioController();

switch ($params[0]) {
    case 'home':
        $materiaController->showHome(); //lista las materias en la home
        break;
    case 'listarProfesores':
        $profesorController->listProfesores();
        break;

    case 'detalleMateria':
        $materiaController->detalleMateria($params[1]);
        break;

    case 'materiasDeProfesor':
        $materiaController->listMateriasDeProfe($params[1]);
        break;
    case 'admin':
        if (isset($params[1])) {
            switch ($params[1]) {
                case 'subirImg':
                    $materiaController->uploadImg();
                    break;
                case 'deleteImg':
                    $path=$params[2] . "/" . $params[3];
                    $materiaController->deleteImg($path);
                    break;
                case 'usuarios':
                    if (isset($params[2]) && ($params[2]=="editarPermisos")){
                        $usuarioController->updatePermisoUsuario($params[3]);
                    }
                    else{
                        $usuarioController->showAdminUsuarios();
                    }
                    break;
                case 'deleteUsuario':
                    $usuarioController->deleteUsuario($params[2]);
                    break;
                case 'profesores':
                    $profesorController->showAdminProfesores();
                    break;
                case 'createMateria':
                    $materiaController->createMateria();
                    break;
                case 'createProfesor':
                    $profesorController->createProfesor();
                    break;
                case 'deleteMateria':
                    $materiaController->deleteMateria($params[2]);
                    break;
                case 'deleteProfesor':
                    $profesorController->deleteProfesor($params[2]);
                    break;
                case 'showEditMateria':
                    $materiaController->showUpdateMateria($params[2]);
                    break;
                case 'showEditProfesor':
                    $profesorController->showUpdateProfesor($params[2]);
                    break;
                case 'updateMateria':
                    $materiaController->updateMateria($params[2]);
                    break;
                case 'updateProfesor':
                    $profesorController->updateProfesor($params[2]);
                    break;
                default:
                    $materiaController->show404();
                    break;
            }
        } else {
            $materiaController->showAdminMaterias(); //listar las materias en admin (default)
        }
        break;
    case 'login':
        if (isset($params[1]) && ($params[1] == "verificar")) {
            $usuarioController->verificarLogin();
        } else {
            $usuarioController->showFormLogin();
        }
        break;
    case 'logout':
        $usuarioController->logout();
        break;
    case 'registro':
        if (isset($params[1]) && ($params[1] == "verificar")) {
            $usuarioController->verificarRegistro();
        } else {
            $usuarioController->showFormRegistro();
        }
        break;
    default:
        $materiaController->show404();
        break;
}
