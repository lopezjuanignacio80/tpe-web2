-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 24, 2021 at 03:18 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tpe`
--

-- --------------------------------------------------------

--
-- Table structure for table `comentarios`
--

CREATE TABLE `comentarios` (
  `id` int(11) NOT NULL,
  `id_materia` int(11) NOT NULL,
  `mail_usuario` varchar(100) NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `puntaje` int(1) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `comentarios`
--

INSERT INTO `comentarios` (`id`, `id_materia`, `mail_usuario`, `descripcion`, `puntaje`, `fecha`) VALUES
(19, 52, 'lopezjuanignacio80@gmail.com', 'Buenas tardes! Estoy probando el comentario..', 5, '2021-11-02'),
(41, 52, 'todopoderoso@gmail.com', 'Hola yo tambien.', 3, '2021-11-07'),
(42, 52, 'todopoderoso@gmail.com', 'Materia mala', 1, '2021-11-07'),
(56, 52, 'todopoderoso@gmail.com', 'mg', 4, '2021-11-24'),
(57, 52, 'lopezjuanignacio80@gmail.com', 'tremendo contenido.', 5, '2021-11-24'),
(58, 54, 'lopezjuanignacio80@gmail.com', 'claves', 4, '2021-11-24');

-- --------------------------------------------------------

--
-- Table structure for table `imagenes`
--

CREATE TABLE `imagenes` (
  `id` int(11) NOT NULL,
  `path` varchar(200) NOT NULL,
  `id_materia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `imagenes`
--

INSERT INTO `imagenes` (`id`, `path`, `id_materia`) VALUES
(14, 'imagenesMateria/618d1395e48701.83357959.jpg', 48),
(15, 'imagenesMateria/618d139d9cc892.59497009.jpeg', 48),
(17, 'imagenesMateria/618d13c2ef0638.96379099.jpeg', 49),
(18, 'imagenesMateria/618d13cc7ebfc4.42757651.jpeg', 52),
(19, 'imagenesMateria/618d14547d3883.47869738.jpg', 54);

-- --------------------------------------------------------

--
-- Table structure for table `materias`
--

CREATE TABLE `materias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(2000) NOT NULL,
  `id_profesor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `materias`
--

INSERT INTO `materias` (`id`, `nombre`, `descripcion`, `id_profesor`) VALUES
(48, 'Programación 1', 'Este curso introduce a los alumnos a los conceptos básicos de programación, principalmente aprendiendo lógica y condiciones, secuencias de ejecución y tipos de datos. Para esto, aprenderá la sintaxis de un lenguaje de programación junto con un entorno de desarrollo visual, de simple uso y que permita el desarrollo rápido de programas.   Al finalizar el curso, los alumnos podrán implementar y ejecutar programas que resuelvan problemas sencillos. El enfoque para encarar problemas de distinto tamaño será a partir de aplicar diferentes metodologías de división de problemas utilizando distintas estructuras de datos básicas.', 44),
(49, 'TMC', 'El Taller de Matemática Computacional pretende brindar a los alumnos contenidos y procedimientos de matemática que sirvan de base para las materias que cursará en la tecnicatura, pero que también en sí mismos sean aportes significativos para su formación como técnicos. Se propone introducir a los conceptos siempre desde la programación, realizando de análisis de funciones y operaciones con vectores, estudiando rangos de valores válidos y representaciones gráficas en pantalla. Aprenderá conceptos de estadística, analizando los resultados utilizando herramientas básicas como el promedio o la varianza, evaluando los datos utilizando distintos criterios o filtros.', 36),
(50, 'Inglés 1', 'No hay información disponible aún', 36),
(51, 'Inglés 2', 'No hay información disponible aún', 39),
(52, 'Web 2', 'Este curso introduce al alumno al modelo de desarrollo para Internet. Para esto, aprenderá nociones generales, tal como lenguajes, navegadores y modelos de comunicaciones. Comenzará con el diseño de páginas web utilizando un lenguaje declarativo, definiendo contenido y formato de datos. Cerca del final del curso, podrá incorporar los conocimientos básicos de programación para añadir contenido dinámico en estas páginas.  Objetivos:  Que el alumno se familiarice con los conceptos y términos básicos del lenguaje declarativo. Que el alumno se introduzca en lenguajes y tecnologías de Internet Contenidos mínimos: Nociones de Internet: Modelos de arquitecturas de la web. Sitios. Procesamiento local y distribuido. Comunicaciones. Introducción a lenguajes para la programación WEB. Lenguaje declarativo. HTML. Sintaxis. Diseño de aplicaciones web dinámicas. Programación Server Side. Diseño de API REST.', 39),
(54, 'Seminario Tecnológico 1', 'La información respecto a seminarios se dará siempre mediante la lista de Coordinación de la carrera. https://groups.google.com/forum/#!forum/tudai-coordinacion', 36),
(86, 'Programación 4', 'esta materia será creada en 50 años..', 44);

-- --------------------------------------------------------

--
-- Table structure for table `profesores`
--

CREATE TABLE `profesores` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `DNI` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `profesores`
--

INSERT INTO `profesores` (`id`, `nombre`, `DNI`) VALUES
(36, 'Juan Sebastian', '22608701'),
(39, 'Roger Federer', '434333313'),
(43, 'Juan Ignacio López', '2160412'),
(44, 'Marcos Migdal', '111112');

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `email` varchar(100) NOT NULL,
  `contraseña` varchar(100) NOT NULL,
  `admin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`email`, `contraseña`, `admin`) VALUES
('lopezjuanignacio80@gmail.com', '$2y$10$cBibd3AByH537sA524eKVeBR1wgd2QiA02003ytNqw5ZoVdR24l9i', 0),
('todopoderoso@gmail.com', '$2a$12$MxM1uVDEzeqsU4ckCnZ3HOpp4DpUYqXXiQyh4u1I3oeOL8PXC34Y2', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_comentario_materia` (`id_materia`),
  ADD KEY `fk_comentario_user` (`mail_usuario`);

--
-- Indexes for table `imagenes`
--
ALTER TABLE `imagenes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_imagen_materia` (`id_materia`);

--
-- Indexes for table `materias`
--
ALTER TABLE `materias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_materia_profesor` (`id_profesor`);

--
-- Indexes for table `profesores`
--
ALTER TABLE `profesores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `imagenes`
--
ALTER TABLE `imagenes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `materias`
--
ALTER TABLE `materias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `profesores`
--
ALTER TABLE `profesores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comentarios`
--
ALTER TABLE `comentarios`
  ADD CONSTRAINT `fk_comentario_materia` FOREIGN KEY (`id_materia`) REFERENCES `materias` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_comentario_user` FOREIGN KEY (`mail_usuario`) REFERENCES `usuarios` (`email`) ON DELETE CASCADE;

--
-- Constraints for table `imagenes`
--
ALTER TABLE `imagenes`
  ADD CONSTRAINT `fk_imagen_materia` FOREIGN KEY (`id_materia`) REFERENCES `materias` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `materias`
--
ALTER TABLE `materias`
  ADD CONSTRAINT `fk_materia_profesor` FOREIGN KEY (`id_profesor`) REFERENCES `profesores` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
