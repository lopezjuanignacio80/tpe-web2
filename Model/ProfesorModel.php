<?php

class ProfesorModel{

    private $db;
    
    function __construct(){
         $this->db = new PDO('mysql:host=localhost;'.'dbname=tpe;charset=utf8', 'root', '');
    }
    
    function getProfesores(){
        $sentencia = $this->db->prepare("SELECT * FROM profesores");
        $sentencia->execute();
        $profesores = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $profesores;
    } 

    function getProfesor($id){
        $sentencia = $this->db->prepare("SELECT * FROM profesores WHERE id=?");
        $sentencia->execute(array($id));
        $profesor= $sentencia->fetch(PDO::FETCH_OBJ);
        return $profesor;
    }
    function createProfesor($nombre,$dni){
        $sentencia = $this->db->prepare("INSERT INTO profesores(nombre, DNI) VALUES(?,?)");
        $sentencia->execute(array($nombre,$dni));
    }
    
    function deleteProfesor($id){
        $sentencia = $this->db->prepare("DELETE FROM profesores WHERE id=?");
        $sentencia->execute(array($id));
    }
    
    function updateProfesor($nombre,$dni,$id){ 
        $sentencia = $this->db->prepare("UPDATE profesores SET nombre=?, DNI=? WHERE id=?");
        $sentencia->execute(array($nombre,$dni,$id));
    }

    function getProfesorDNI($dni){
        $sentencia = $this->db->prepare("SELECT * FROM profesores WHERE DNI=?");
        $sentencia->execute(array($dni));
        $profesor= $sentencia->fetch(PDO::FETCH_OBJ);
        return $profesor;
    }
    
}