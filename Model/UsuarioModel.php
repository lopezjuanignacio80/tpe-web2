<?php
class UsuarioModel
{

    private $db;

    function __construct()
    {
        $this->db = new PDO('mysql:host=localhost;' . 'dbname=tpe;charset=utf8', 'root', '');
    }

    function createUsuario($email, $password,$valor) 
    {
        $sentencia = $this->db->prepare("INSERT INTO usuarios(email,contraseña,admin) VALUES(?, ?, ?)");
        $sentencia->execute(array($email, $password, $valor));
    }

    function existeUsuario($email)
    {
        $sentencia = $this->db->prepare("SELECT * FROM usuarios WHERE email=?");
        $sentencia->execute(array($email));
        $usuario = $sentencia->fetch(PDO::FETCH_OBJ);
        return $usuario;
    }
    function getUsuarios()
    {
        $sentencia = $this->db->prepare("SELECT * FROM usuarios");
        $sentencia->execute();
        $usuarios = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $usuarios;
    }
    function getUsuario($id)
    {
        $sentencia = $this->db->prepare("SELECT * FROM usuarios WHERE email=?");
        $sentencia->execute(array($id));
        $usuario = $sentencia->fetch(PDO::FETCH_OBJ);
        return $usuario;
    }
    function deleteUsuario($id)
    {
        $sentencia = $this->db->prepare("DELETE FROM usuarios WHERE email=?");
        $sentencia->execute(array($id));
    }
    function updateUsuario($id,$permiso)
    {
        $sentencia = $this->db->prepare("UPDATE usuarios SET admin=? WHERE email=?");
        $sentencia->execute(array($permiso,$id));
    }

}
