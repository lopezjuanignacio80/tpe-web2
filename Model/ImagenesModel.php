<?php
class ImagenesModel{

    private $db;

    function __construct(){
        $this->db = new PDO('mysql:host=localhost;'.'dbname=tpe;charset=utf8', 'root', '');
    }
    function getImagenes()
    {
        $sentencia = $this->db->prepare("SELECT * FROM imagenes");
        $sentencia->execute();
        $imagenes = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $imagenes;
    }

    function addImagen($id,$filePath){
        $sentencia_img=$this->db->prepare("INSERT INTO imagenes(path,id_materia) VALUES(?, ?)");
        $sentencia_img->execute(array($filePath,$id));
    }
    function deleteImg($path){
        $sentencia = $this->db->prepare("DELETE FROM imagenes WHERE path=?"); //es lo mismo que eliminar por id, son únicos
        $sentencia->execute(array($path));
    }
    function getImgsMateria($id_materia){
        $sentencia = $this->db->prepare("SELECT * FROM imagenes WHERE id_materia=?");
        $sentencia->execute(array($id_materia));
        $imagenes = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $imagenes;
    }
}