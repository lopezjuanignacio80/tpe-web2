<?php

class MateriaModel
{

    private $db;

    function __construct()
    {
        $this->db = new PDO('mysql:host=localhost;' . 'dbname=tpe;charset=utf8', 'root', '');
    }

    function getMaterias()
    {
        $sentencia = $this->db->prepare("SELECT * FROM materias");
        $sentencia->execute();
        $materias = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $materias;
    }


    function getMateriasProfe($id_profesor)
    {
        $sentencia = $this->db->prepare("SELECT * FROM materias WHERE id_profesor=?");
        $sentencia->execute(array($id_profesor));
        $materias = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $materias;
    }

    function getMateria($id)
    {
        $sentencia = $this->db->prepare("SELECT materias.*, profesores.nombre as nombreProfesor FROM materias JOIN profesores ON materias.id_profesor = profesores.id WHERE materias.id=?");
        $sentencia->execute(array($id));
        $materia = $sentencia->fetch(PDO::FETCH_OBJ);
        return $materia;
    }

    function createMateria($nombre, $descripcion, $id_profesor)
    {
        $sentencia = $this->db->prepare("INSERT INTO materias(nombre, descripcion, id_profesor) VALUES(?, ?, ?)");
        $sentencia->execute(array($nombre, $descripcion, $id_profesor));
    }

    function deleteMateria($id)
    {
        $sentencia = $this->db->prepare("DELETE FROM materias WHERE id=?");
        $sentencia->execute(array($id));
    }

    function updateMateria($nombre, $descripcion, $id_profesor, $id)
    {
        $sentencia = $this->db->prepare("UPDATE materias SET nombre=?, descripcion=?, id_profesor=? WHERE id=?");
        $sentencia->execute(array($nombre, $descripcion, $id_profesor, $id));
    }

    function getMateriaIgual($nombre, $id_profesor)
    {
        $sentencia = $this->db->prepare("SELECT * FROM materias WHERE nombre=? AND id_profesor=?");
        $sentencia->execute(array($nombre, $id_profesor));
        $materia = $sentencia->fetch(PDO::FETCH_OBJ);
        return $materia;
    }
    function getMateriasPaginadas($limit,$offset){
        //offset skipea la cantidad de filas que este seteada. por ej si es 10 el offset, comienza a partir del 11
        $sentencia = $this->db->prepare("SELECT * FROM materias LIMIT $limit OFFSET $offset");
        $sentencia->execute();
        $materias = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $materias;

    }
    
    function getMateriasPaginadasFiltradasByNombre($limit,$offset,$nombreFiltrado){
        $sentencia = $this->db->prepare("SELECT * FROM materias  WHERE nombre LIKE ? LIMIT $limit OFFSET $offset");
        $sentencia->execute(array("$nombreFiltrado%"));
        $materias = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $materias;
    }
    function getMateriasPaginadasFiltradasByDescripcion($limit,$offset,$descripcionFiltrado){
        $sentencia = $this->db->prepare("SELECT * FROM materias WHERE descripcion LIKE ? LIMIT $limit OFFSET $offset");
        $sentencia->execute(array("$descripcionFiltrado%"));
        $materias = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $materias;
    }
    function getMateriasFiltradasByNombre($nombreFiltrado){
        $sentencia = $this->db->prepare("SELECT * FROM materias WHERE nombre LIKE ?");
        $sentencia->execute(array("$nombreFiltrado%"));
        $materias = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $materias;
    }
    function getMateriasFiltradasByDescripcion($descripcionFiltrado){
        $sentencia = $this->db->prepare("SELECT * FROM materias WHERE descripcion LIKE ?");
        $sentencia->execute(array("$descripcionFiltrado%"));
        $materias = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $materias;
    }
}
