<?php
class ComentarioModel{

    private $db;

    function __construct(){
        $this->db = new PDO('mysql:host=localhost;'.'dbname=tpe;charset=utf8', 'root', '');
    }
    function getComentarios()
    {
        $sentencia = $this->db->prepare("SELECT * FROM comentarios");
        $sentencia->execute();
        $comentarios = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $comentarios;
    }

    function getComentario($id)
    {
        $sentencia = $this->db->prepare("SELECT * FROM comentarios WHERE id=?");
        $sentencia->execute(array($id));
        $comentario = $sentencia->fetch(PDO::FETCH_OBJ);
        return $comentario;
    }

    function addComentario($id_materia, $mail_usuario, $descripcion, $puntaje,$fecha)
    {
        $sentencia = $this->db->prepare("INSERT INTO comentarios(id_materia, mail_usuario, descripcion, puntaje, fecha) VALUES(?, ?, ?, ?, ?)");
        $sentencia->execute(array($id_materia, $mail_usuario, $descripcion, $puntaje,$fecha));
        return $this->db->lastInsertId();
    }
    function updateComentario($id_materia, $mail_usuario, $descripcion, $puntaje,$fecha,$id){
        $sentencia = $this->db->prepare("UPDATE comentarios SET id_materia=?, mail_usuario=?, descripcion=?, puntaje=?, fecha=? WHERE id=?");
        $sentencia->execute(array($id_materia, $mail_usuario, $descripcion, $puntaje,$fecha,$id));
    }
    function deleteComentario($id)
    {
        $sentencia = $this->db->prepare("DELETE FROM comentarios WHERE id=?");
        $sentencia->execute(array($id));
    }

    function getComentariosDeMateria($id){
        $sentencia = $this->db->prepare("SELECT * FROM comentarios WHERE id_materia=?");
        $sentencia->execute(array($id));
        $comentarios = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $comentarios;
    }
    function getComentariosFiltradosOrdenados($sort,$order,$puntaje,$idMateria){
        $sentencia = $this->db->prepare("SELECT * FROM comentarios WHERE puntaje=? AND id_materia=? ORDER BY $sort $order");
        $sentencia->execute(array($puntaje,$idMateria));
        $comentarios = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $comentarios;
    }
    function getComentariosOrdenados($sort,$order,$idMateria){
        $sentencia = $this->db->prepare("SELECT * FROM comentarios WHERE id_materia=? ORDER BY $sort $order");
        $sentencia->execute(array($idMateria));
        $comentarios = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $comentarios;
    }
    function getComentariosFiltrados($puntaje,$idMateria){
        $sentencia = $this->db->prepare("SELECT * FROM comentarios WHERE puntaje=? AND id_materia=?");
        $sentencia->execute(array($puntaje,$idMateria));
        $comentarios = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $comentarios;
    }
}