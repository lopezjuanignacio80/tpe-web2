<?php

require_once "Model/UsuarioModel.php";
require_once "View/UsuarioView.php";
require_once "Helpers/AuthHelper.php";

class UsuarioController
{
    private $model;
    private $view;
    private $authHelper;

    function __construct()
    {
        $this->model = new UsuarioModel();
        $this->view = new UsuarioView();
        $this->authHelper = new AuthHelper();
    }

    function showFormLogin()
    {
        $this->authHelper->checkRegistrado();
        $this->view->showFormLogin();
    }

    function showFormRegistro()
    {
        $this->authHelper->checkRegistrado();
        $this->view->showFormRegistro();
    }

    function verificarRegistro()
    {
        $this->authHelper->checkRegistrado();
        if (isset($_POST['email']) && isset($_POST['password']) && isset($_POST['password-repeat'])) {
            $email = $_POST['email'];
            $usuario = $this->model->existeUsuario($email);
            $password = $_POST['password'];
            $passwordRepeat = $_POST['password-repeat'];
            if (!$usuario) { //si el usuario no existe en el sistema, puede registrarse
                if ($password <> $passwordRepeat) {
                    $this->view->showFormRegistro(true, $email, $password, $passwordRepeat);
                } else {
                    $passwordEncrypt = password_hash($password, PASSWORD_DEFAULT);
                    $this->model->createUsuario($email, $passwordEncrypt,0); //solo usuarios no admin
                    session_start();
                    $_SESSION["email"] = $email;
                    $this->view->showSuccessRegistro();
                }
            } else {
                $this->view->showFormRegistro(false, $email, $password, $passwordRepeat, true);
            }
        }
    }

    function verificarLogin()
    {
        $this->authHelper->checkRegistrado();
        if (isset($_POST['email']) && isset($_POST['password'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];
            $usuario = $this->model->existeUsuario($email);
            if (($usuario) && password_verify($password, $usuario->contraseña)) {
                session_start();
                $_SESSION["email"] = $email;
                if ($usuario->admin == 1) {
                    $_SESSION["admin"] = true;
                }
                $this->view->showHomeLocation();
            } else {
                if (!$usuario) {
                    //error usuario no existe
                    $this->view->showFormLogin(true, false, $email, $password);
                } else {
                    //ultimo caso, usuario existe pero contraseña incorrecta
                    $this->view->showFormLogin(false, true, $email, $password);
                }
            }
        }
    }
    function showAdminUsuarios()
    {
        $this->authHelper->checkAdmin();
        $usuarios = $this->model->getUsuarios();
        $this->view->showUsuarios($usuarios);
    }

    function logout()
    {
        $this->authHelper->logout();
    }

    function deleteUsuario($id)
    {
        $this->authHelper->checkAdmin();
        $usuario = $this->model->getUsuario($id);
        if ($usuario != null) {
            $this->model->deleteUsuario($id);
            $this->view->showAdminUsuariosLocation();
        } else {
            $this->view->showUsuarioFail();
        }
    }

    function updatePermisoUsuario($id)
    {
        $this->authHelper->checkAdmin();
        $usuario = $this->model->getUsuario($id);
        if (isset($_POST["checkbox"])) {
            $permiso = 1;
        } else {
            $permiso = 0;
        }
        if ($usuario != null) {
            $this->model->updateUsuario($id, $permiso);
            $this->view->showAdminUsuariosLocation();
        } else {
            $this->view->showUsuarioFail();
        }
    }
}
