<?php

require_once "Model/MateriaModel.php";
require_once "View/MateriaView.php";
require_once "Helpers/AuthHelper.php";
require_once "Model/ImagenesModel.php";

class MateriaController
{
    private $model;
    private $view;
    private $profesorModel;
    private $profesorView;
    private $authHelper;
    private $imagenesModel;

    function __construct()
    {
        $this->model = new MateriaModel();
        $this->view = new MateriaView();
        $this->profesorModel = new ProfesorModel();
        $this->profesorView = new ProfesorView();
        $this->authHelper = new AuthHelper();
        $this->imagenesModel= new ImagenesModel();
    }
    
    function showHome()
    {
        session_start();
        $materias = $this->model->getMaterias();
        if (!isset($_SESSION["email"])) {
            $this->view->showMaterias($materias);
        } else {
            if (isset($_GET['filtrarNombre'])){
                $materias = $this->model->getMateriasFiltradasByNombre($_GET['filtrarNombre']);  
            }else{
                if (isset($_GET['filtrarDescripcion'])){
                    $materias = $this->model->getMateriasFiltradasByDescripcion($_GET['filtrarDescripcion']); 
                }
            }
            //paginacion
            $limit=3;//materias por página
            $pagActual=1; 
            $cantMateriasTotal=count($materias);
            $cantPags=$cantMateriasTotal/$limit;
            if (is_double($cantPags)){ //si son 26 materias y divido sobre 5, da 5 y pico, necesito otra página
                $cantPags=(int)$cantPags;
                $cantPags+=1;
            }
            if(isset($_GET['nroPag'])){
                if ($_GET['nroPag']>=1 && $_GET['nroPag']<=$cantPags){
                    $pagActual=$_GET['nroPag'];
                }
            }
            $offset=($pagActual-1)*$limit;
            if (isset($_GET['filtrarNombre'])){
                $materias = $this->model->getMateriasPaginadasFiltradasByNombre($limit,$offset,$_GET['filtrarNombre']);  
                if (isset($_SESSION["admin"])){
                    $this->view->showMaterias($materias,null,$_GET['filtrarNombre'],$cantPags,$pagActual, true,true);
                }else{
                    //logueado como usuario, no admin
                    $this->view->showMaterias($materias,null,$_GET['filtrarNombre'],$cantPags,$pagActual, true);
                }
            }else{
                if (isset($_GET['filtrarDescripcion'])){
                    $materias = $this->model->getMateriasPaginadasFiltradasByDescripcion($limit,$offset,$_GET['filtrarDescripcion']);  
                    if (isset($_SESSION["admin"])){
                        $this->view->showMaterias($materias,$_GET['filtrarDescripcion'],null,$cantPags,$pagActual, true,true);
                    }else{
                        //logueado como usuario, no admin
                        $this->view->showMaterias($materias,$_GET['filtrarDescripcion'],null,$cantPags,$pagActual, true);
                    }
                }else{
                    $materias = $this->model->getMateriasPaginadas($limit,$offset); 
                    if (isset($_SESSION["admin"])){
                        $this->view->showMaterias($materias,null,null,$cantPags,$pagActual, true,true);
                    }else{
                        //logueado como usuario, no admin
                        $this->view->showMaterias($materias,null,null,$cantPags,$pagActual, true);
                    }
                }
            }
        }
    }

    
    function detalleMateria($id)
    {
        session_start();
        $materia = $this->model->getMateria($id);
        if ($materia != null) {
            $imagenes=$this->imagenesModel->getImgsMateria($id);
            if (isset($_SESSION["admin"])){
                $this->view->showMateria($imagenes,$materia,true,true);
            }else{
                if (!isset($_SESSION["email"])) {
                    $this->view->showMateria($imagenes,$materia);
                }
                else{
                    $this->view->showMateria($imagenes,$materia, true);
                }
            }  
        }
        else{
            if (isset($_SESSION["admin"])){
                $this->view->showMateriaFail(true,true);
            }else{
                if (!isset($_SESSION["email"])) {
                    $this->view->showMateriaFail();
                }
                else{
                    $this->view->showMateriaFail(true);
                }
            }
        }
    }
    
    function listMateriasDeProfe($id_profesor)
    {
        session_start();
        $profesor = $this->profesorModel->getProfesor($id_profesor);
        if ($profesor != null) {
            $materias = $this->model->getMateriasProfe($id_profesor);
            if (isset($_SESSION["admin"])){
                $this->view->showMateriasProfe($materias, $profesor,true,true); 
            }
            else{
                if (!isset($_SESSION["email"])) {
                    $this->view->showMateriasProfe($materias, $profesor);
                }
                else
                $this->view->showMateriasProfe($materias, $profesor, true);
            }
            
        }else{
            if (isset($_SESSION["admin"])){
                $this->profesorView->showProfesorFail(true,true);
            }
            else{
                if (!isset($_SESSION["email"])) {
                    $this->profesorView->showProfesorFail();
                }else{
                    $this->profesorView->showProfesorFail(true);
                }
            }
        }
    }
    
    function showAdminMaterias()
    {
        $this->authHelper->checkNoRegistrado();
        $materias = $this->model->getMaterias();
        $profesores = $this->profesorModel->getProfesores();
        $imagenes=$this->imagenesModel->getImagenes();
        if (isset($_SESSION["admin"])){
            $this->view->showAdminMaterias($imagenes,$materias, $profesores,null,true);
        }
        else{
            $this->view->showAdminMaterias(null,$materias, $profesores);
        }
            
    }

    function createMateria()
    //Criterio de agregado: Se considera repetida la materia si se ingresa el mismo nombre y el mismo profesor
    //es decir, está permitido ingresar la misma materia pero con otro profesor
    {
        $this->authHelper->checkNoRegistrado();
        if (isset($_POST['nombre']) && isset($_POST['profesor']) && isset($_POST['descripcion'])) {
            $mismaMateria = $this->model->getMateriaIgual($_POST['nombre'], $_POST['profesor']);
            if ($mismaMateria == null) {
                $this->model->createMateria($_POST['nombre'], $_POST['descripcion'], $_POST['profesor']);
                $this->view->showAdminLocation();
            } else {
                $materias = $this->model->getMaterias();
                $profesores = $this->profesorModel->getProfesores();
                $imagenes=$this->imagenesModel->getImagenes();
                if (isset($_SESSION["admin"])){
                    $this->view->showAdminMaterias($imagenes,$materias, $profesores,true,true);
                }
                else{
                    $this->view->showAdminMaterias(null,$materias, $profesores,true);
                }
            }
        } else {
            $materias = $this->model->getMaterias();
            $profesores = $this->profesorModel->getProfesores();
            $imagenes=$this->imagenesModel->getImagenes();
            if (isset($_SESSION["admin"])){
                $this->view->showAdminMaterias($imagenes,$materias, $profesores,null,true);
            }
            else{
                $this->view->showAdminMaterias(null,$materias, $profesores);
            }
        }
    }

    function deleteMateria($id)
    {
        $this->authHelper->checkNoRegistrado();
        $materia = $this->model->getMateria($id);
        if ($materia != null) {
            $this->model->deleteMateria($id);
            $this->view->showAdminLocation();
        } else {
            if (isset($_SESSION["admin"])){
                $this->view->showMateriaFail(true,true);
            }
            else{
                $this->view->showMateriaFail(true);
            }
        }
    }

    function updateMateria($id)
    //Tener en cuenta: no puedo tratar de editar una materia con los datos de otra materia ya cargada
    {
        $this->authHelper->checkNoRegistrado();
        $materia = $this->model->getMateria($id);
        if ($materia != null) {
            if (isset($_POST['nombre']) && isset($_POST['profesor']) && isset($_POST['descripcion'])) {
                $mismaMateria = $this->model->getMateriaIgual($_POST['nombre'], $_POST['profesor']);
                if ((($mismaMateria) && ($mismaMateria->nombre == $materia->nombre) && ($mismaMateria->id_profesor == $materia->id_profesor))
                    || ($mismaMateria == null)
                ) {
                    $this->model->updateMateria($_POST['nombre'], $_POST['descripcion'], $_POST['profesor'], $id);
                    $this->view->showAdminLocation();
                } else {
                    $materias = $this->model->getMaterias();
                    $profesores = $this->profesorModel->getProfesores();
                    $imagenes=$this->imagenesModel->getImagenes();
                    if (isset($_SESSION["admin"])){
                        $this->view->showFormEdit($imagenes,$materias, $profesores, $materia, true,true);
                    }else
                    $this->view->showFormEdit(null,$materias, $profesores, $materia, true);
                }
            } else {
                $materias = $this->model->getMaterias();
                $profesores = $this->profesorModel->getProfesores();
                $imagenes=$this->imagenesModel->getImagenes();
                if (isset($_SESSION["admin"])){
                    $this->view->showFormEdit($imagenes,$materias, $profesores, $materia,null,true);
                }
                else
                    $this->view->showFormEdit(null,$materias, $profesores, $materia);
            }
        } else {
            if (isset($_SESSION["admin"])){
                $this->view->showMateriaFail(true,true);
            }
            else{
                $this->view->showMateriaFail(true);
            }
        }
    }

    function showUpdateMateria($id)
    {
        $this->authHelper->checkNoRegistrado();
        $materia = $this->model->getMateria($id);
        if ($materia != null) {
            $materias = $this->model->getMaterias();
            $profesores = $this->profesorModel->getProfesores();
            $imagenes=$this->imagenesModel->getImagenes();
            if (isset($_SESSION["admin"])){
                $this->view->showFormEdit($imagenes,$materias, $profesores, $materia,null,true);
            }
            else
                $this->view->showFormEdit(null,$materias, $profesores, $materia);
        } else {
            if (isset($_SESSION["admin"])){
                $this->view->showMateriaFail(true,true);
            }
            else{
                $this->view->showMateriaFail(true);
            }
        }
    }

    function show404(){
        $this->view->show404();
    }


    function uploadImg(){
        $this->authHelper->checkAdmin();
        if(isset($_POST['materia'])){
            foreach($_FILES["imagesToUpload"]["tmp_name"] as $key => $tmp_name)
            {
                if(($_FILES['imagesToUpload']['type'][$key] != "image/jpg" && $_FILES['imagesToUpload']['type'][$key] != "image/jpeg" && $_FILES['imagesToUpload']['type'][$key] != "image/png")){
                    $error=true;   
                }
            }
            if (!$error){
                foreach($_FILES["imagesToUpload"]["tmp_name"] as $key => $tmp_name)
                {   
                    $filePath = "imagenesMateria/" . uniqid("", true) . "." 
                    . strtolower(pathinfo($_FILES['imagesToUpload']['name'][$key], PATHINFO_EXTENSION));
                    var_dump(move_uploaded_file($tmp_name, $filePath));
                    $this->imagenesModel->addImagen($_POST['materia'],$filePath);
                }
                $this->view->showAdminLocation();
            }
            else{    
                $materias = $this->model->getMaterias();
                $profesores = $this->profesorModel->getProfesores();
                $imagenes=$this->imagenesModel->getImagenes();
                $this->view->showAdminMaterias($imagenes,$materias, $profesores,false,true,true);
            }
        }
    }
    function deleteImg($path){
        $this->authHelper->checkAdmin();
        $this->imagenesModel->deleteImg($path);
        unlink($path); //elimina el archivo fisico 
        $this->view->showAdminLocation();
    }
}
