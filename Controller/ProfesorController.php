<?php

require_once "Model/ProfesorModel.php";
require_once "View/ProfesorView.php";
require_once "Helpers/AuthHelper.php";

class ProfesorController
{

    private $model;
    private $view;
    private $materiaModel;
    private $authHelper;

    function __construct()
    {
        $this->model = new ProfesorModel();
        $this->view = new ProfesorView();
        $this->materiaModel = new MateriaModel();
        $this->authHelper = new AuthHelper();
    }

    function listProfesores()
    {
        session_start();
        $profesores = $this->model->getProfesores();
        if (isset($_SESSION["admin"])){
            $this->view->showProfesores($profesores,true,true);
        }else{
            if (!isset($_SESSION["email"])) {
                $this->view->showProfesores($profesores);
            } else {
                $this->view->showProfesores($profesores, true);
            }
        }
    }


    function showAdminProfesores()
    {
        $this->authHelper->checkNoRegistrado();
        $profesores = $this->model->getProfesores();
        if (isset($_SESSION["admin"])) {
            $this->view->showAdminProfesores($profesores, false, false,true);  
        }
        else{
            $this->view->showAdminProfesores($profesores, false, false);
        }
    }

    function createProfesor()
    {
        $this->authHelper->checkNoRegistrado();
        if (isset($_POST['DNI']) && isset($_POST['nombre'])) {
            $profesorMismoDNI = $this->model->getProfesorDNI($_POST['DNI']);
            if ($profesorMismoDNI == null) { // si no existe otro profe con mismo DNI puedo agregarlo
                $this->model->createProfesor($_POST['nombre'], $_POST['DNI']);
                $this->view->showAdminLocation();
            } else {
                $profesores = $this->model->getProfesores();
                if (isset($_SESSION["admin"])) {
                    $this->view->showAdminProfesores($profesores, null, true,true);  
                }
                else{
                    $this->view->showAdminProfesores($profesores, null, true);
                }
                
            }
        } else {
            $profesores = $this->model->getProfesores();
            if (isset($_SESSION["admin"])) {
                $this->view->showAdminProfesores($profesores, null, null,true);  
            }
            else{
                $this->view->showAdminProfesores($profesores);
            }
        }
    }

    function deleteProfesor($id)
    {
        $this->authHelper->checkNoRegistrado();
        $profesor = $this->model->getProfesor($id);
        if ($profesor != null) {
            $profesores = $this->model->getProfesores();
            $materiasProfesor = $this->materiaModel->getMateriasProfe($id);
            if ($materiasProfesor <> null) { //el profesor tiene materias, no debo borrarlo
                $profesorError = $profesor; //solo cambia de nombre para entender el contexto
                if (isset($_SESSION["admin"])) {
                    $this->view->showAdminProfesores($profesores, $profesorError, null,true);  
                }
                else{
                    $this->view->showAdminProfesores($profesores, $profesorError);
                }
            } else {
                $this->model->deleteProfesor($id);
                $this->view->showAdminLocation();
            }
        } else {
            if (isset($_SESSION["admin"])) {
                $this->view->showProfesorFail(true,true);
            }
            else{
                $this->view->showProfesorFail(true);
            }
        }
    }

    function updateProfesor($id)
    {
        $this->authHelper->checkNoRegistrado();
        $profesor = $this->model->getProfesor($id);
        if ($profesor != null) {
            if (isset($_POST['DNI']) && isset($_POST['nombre'])) {
                $profesorMismoDNI = $this->model->getProfesorDNI($_POST['DNI']);
                if ((($profesorMismoDNI) && ($profesorMismoDNI->DNI == $profesor->DNI)) || ($profesorMismoDNI == null)) {
                    //si se esta cambiando el DNI al editar, no puede ingresar otro DNI en uso
                    $this->model->updateProfesor($_POST['nombre'], $_POST['DNI'], $id);
                    $this->view->showAdminLocation();
                } else {
                    $profesores = $this->model->getProfesores();
                    if (isset($_SESSION["admin"])) {
                        $this->view->showFormEdit($profesores, $profesor, true,true);  
                    }
                    else{
                        $this->view->showFormEdit($profesores, $profesor, true);
                    } 
                }
            } else {
                $profesores = $this->model->getProfesores();
                if (isset($_SESSION["admin"])) {
                    $this->view->showFormEdit($profesores, $profesor, null,true);  
                }
                else{
                    $this->view->showFormEdit($profesores, $profesor);
                } 
            }
        } else {
            if (isset($_SESSION["admin"])) {
                $this->view->showProfesorFail(true,true);
            }
            else{
                $this->view->showProfesorFail(true);
            }
        }
    }

    function showUpdateProfesor($id)
    {
        $this->authHelper->checkNoRegistrado();
        $profesor = $this->model->getProfesor($id);
        if ($profesor != null) {
            $profesores = $this->model->getProfesores();
            if (isset($_SESSION["admin"])) {
                $this->view->showFormEdit($profesores, $profesor, null,true);  
            }
            else{
                $this->view->showFormEdit($profesores, $profesor);
            } 
        } else {
            if (isset($_SESSION["admin"])) {
                $this->view->showProfesorFail(true,true);
            }
            else{
                $this->view->showProfesorFail(true);
            }
        }
    }
}
