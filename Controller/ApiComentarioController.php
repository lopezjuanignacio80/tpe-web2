<?php
require_once "Model/ComentarioModel.php";
require_once "View/ApiView.php";
require_once "Model/MateriaModel.php";
require_once "Model/UsuarioModel.php";
class ApiComentarioController{

    private $model;
    private $view;
    private $materiaModel;
    private $usuarioModel;

    function __construct(){
        $this->model = new ComentarioModel();
        $this->view = new ApiView();
        $this->materiaModel = new MateriaModel();
        $this->usuarioModel = new UsuarioModel();
    }

    function getComentarios(){   
        if (isset($_GET['id_materia'])){
            $idMateria=$_GET['id_materia'];
            $materia = $this->materiaModel->getMateria($idMateria);
            if ($materia!=null){
                if (isset($_GET['sort'])){
                    if (isset($_GET['order'])){
                        $order=$_GET['order'];
                        if ($order!='desc' && $order!='asc'){
                            return $this->view->response("ingrese ordenamiento valido", 400); 
                        }
                    }
                    if (!$order){
                        $order="asc";
                    }
                    $sort=$_GET['sort'];
                    if ($sort==='puntaje' || $sort==='fecha'){
                        if (isset($_GET['puntaje'])){
                            $puntaje=$_GET['puntaje'];
                            if ($puntaje>=1 && $puntaje<=5){
                                $comentarios=$this->model->getComentariosFiltradosOrdenados($sort,$order,$puntaje,$idMateria);
                                return $this->view->response($comentarios, 200); 
                            }else{
                                return $this->view->response("ingrese puntaje valido", 400); 
                            }
                        }
                        $comentarios=$this->model->getComentariosOrdenados($sort,$order,$idMateria); 
                        return $this->view->response($comentarios, 200); 
                    }
                    else{
                        return $this->view->response("ingrese sort valido", 400); 
                    }
                }
                if (isset($_GET['puntaje'])){
                    $puntaje=$_GET['puntaje'];
                    if ($puntaje>=1 && $puntaje<=5){
                        $comentarios=$this->model->getComentariosFiltrados($puntaje,$idMateria);
                        return $this->view->response($comentarios, 200); 
                    }else{
                        return $this->view->response("ingrese puntaje valido", 400); 
                    }
                }
                $comentarios = $this->model->getComentariosDeMateria($idMateria);
                return $this->view->response($comentarios, 200); 
            }
            else{
                return $this->view->response("La materia con el id=$idMateria no existe", 404);
            }       
        }
        $comentarios = $this->model->getComentarios();
        return $this->view->response($comentarios, 200);
    }

    function getComentario($params = null){
        $idComentario = $params[":ID"];
        $comentario = $this->model->getComentario($idComentario);
        if ($comentario) {
            return $this->view->response($comentario, 200);
        } else {
            return $this->view->response("El comentario con el id=$idComentario no existe", 404);
        }
        
    }
    function updateComentario($params = null){
        $body = $this->getBody();
        $idComentario = $params[":ID"];
        $comentario = $this->model->getComentario($idComentario);
        if ($comentario) {
            if (isset($body) && isset($body->id_materia) && isset($body->mail_usuario) 
                && isset($body->descripcion)&& isset($body->puntaje) && isset($body->fecha) && ($body->puntaje>=1 && $body->puntaje<=5)){
                $usuario= $this->usuarioModel->existeUsuario($body->mail_usuario);
                $materia= $this->materiaModel->getMateria($body->id_materia);
                if ($usuario && $materia){
                    $this->model->updateComentario($body->id_materia, $body->mail_usuario,
                    $body->descripcion,$body->puntaje,$body->fecha,$idComentario);
                    return $this->view->response("El comentario se editó con éxito", 200);
                }else{
                   return $this->view->response("Mal ingreso", 400);
                }
            }else{
                return $this->view->response("Mal ingreso", 400);
            }
        }else {
            return $this->view->response("El comentario con el id=$idComentario no existe", 404);
        }
    }
    function deleteComentario($params = null){
        $idComentario = $params[":ID"];
        $comentario = $this->model->getComentario($idComentario);
        if ($comentario) {
            $this->model->deleteComentario($idComentario);
            return $this->view->response("El comentario con el id=$idComentario fue borrado", 200);
        } else {
            return $this->view->response("El comentario con el id=$idComentario no existe", 404);
        }
    }

    function addComentario(){
        // obtengo el body del request (json)
        $body = $this->getBody();
        // TODO: VALIDACIONES -> 400 (Bad Request)
        if (isset($body) && isset($body->id_materia) && isset($body->mail_usuario) 
        && isset($body->descripcion)&& isset($body->puntaje) && isset($body->fecha) && ($body->puntaje>=1 && $body->puntaje<=5)){
            $usuario= $this->usuarioModel->existeUsuario($body->mail_usuario);
            $materia= $this->materiaModel->getMateria($body->id_materia);
            if ($materia && $usuario){
                $idComentario = $this->model->addComentario($body->id_materia, $body->mail_usuario,
                $body->descripcion,$body->puntaje,$body->fecha);
                if ($idComentario != null) {
                    return $this->view->response("El comentario se insertó con el id=$idComentario", 200);
                } else {
                    return $this->view->response("El comentario no se pudo insertar", 500);
                }
            }
            else{
                return $this->view->response("Mal ingreso", 400); 
            }
        }
        else{
            return $this->view->response("Mal ingreso", 400);
        }
    }

    private function getBody() {
        $bodyString = file_get_contents("php://input");
        return json_decode($bodyString);
    }
}