{include file='templates/main/header.tpl'}
{include file='templates/main/navbar.tpl'}
<h1 class="titulos">TUDAI | Plan de Estudios</h1>
<p>Bienvenidos al plan de estudios de la carrerra Tec. Univesitaria en Desarrollo de Aplicaciones Informáticas.</p>
<img src="img/home.png" class="imagen-home">

<div class="div-list">
    {if $mail}
        <div class="input-filtro">
            <input type="text" placeholder="search by name" id="input-filtro-nombre" value="{$valorInputNombre}">
        </div>
        <br>
        <div class="input-filtro">
            <input type="text" placeholder="search by description" id="input-filtro-descripcion" value="{$valorInputDescripcion}">
        </div>
    {/if}
    <ul id="list-materias-home">
        {foreach from=$materias item=$materia}
            <a href='detalleMateria/{$materia->id}'><li class="li-materias">{$materia->nombre}</li></a>
        {/foreach}
    </ul>
    {if $mail}
        <ul class="pagination">
            <li class="page-item"><a class="page-link bg-dark text-white" data-paginado="{$pagActual-1}"><<</a></li>
            {for $cant=1 to $cantPags}
                <li class="page-item"><a class="page-link text-white{if $cant==$pagActual} bg-secondary {else} bg-dark {/if}" data-paginado="{$cant}">{$cant}</a></li>
                
            {/for}
            <li class="page-item"><a class="page-link bg-dark text-white" data-paginado="{$pagActual+1}">>></a></li>
        </ul>
    {/if}
</div>
<script src="js/filtro-paginado.js"></script>
{include file='templates/main/footer.tpl'}