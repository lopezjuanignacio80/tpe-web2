 <nav class="navbar navbar-dark bg-dark sticky-top" id="navbar">
  <div class="container-fluid ">
    <div class="navbar-header d-flex">
      <a class="navbar-brand" href="home">HOME</a>
      <a class="nav-item nav-link" href="listarProfesores">PROFESORES</a>
      {if $usuarioLogueado}
      <a class="nav-item nav-link" href="admin">ABM</a>
      {/if}
      {if $admin}
      <a class="nav-item nav-link" href="admin/usuarios">Usuarios</a>
      {/if}

    </div>
      {if $usuarioLogueado}
        {if $mail}
          <span class="span-nav">You are log in as {$mail}</span>
        {/if}
      {/if}
    <div class="nav navbar-right">
    {if $usuarioLogueado}
      <a class="nav-item nav-link btn btn-warning" href="logout">LogOut</a>
    {else}
      <a class="nav-item nav-link btn btn-warning" href="login">SIGN IN</a>
    {/if}
    </div>
  </div>
</nav>

