{include file='templates/main/header.tpl'}
{include file='templates/main/navbar.tpl'}
<h1 class="titulos">Profesores</h1>
<p>Ellos. Los que se encargan de hacerte crecer como desarrollador y persona.</p>
<img src="img/profesores.png" class="imagen-profesores">
<div class="div-list">
    <ul>   
    {foreach from=$profesores item=$profesor}
        <li class="li-profesores">{$profesor->nombre}</li><a href='materiasDeProfesor/{$profesor->id}' class="li-materias">Ver materias</a>
    {/foreach}
    </ul>
</div>
{include file='templates/main/footer.tpl'}