{include file='templates/main/header.tpl'}
{include file='templates/main/navbar.tpl'}
{if $materias}
    <div class="div-list">
        <h2>Materias de {$profesor->nombre}</h2>
        <ul> 
        {foreach from=$materias item=$materia}
            <li class="li-materias">{$materia->nombre}</li>
        {/foreach}
        </ul>
    </div> 
{else}
    <div class="div-list">
        <h2>El profesor {$profesor->nombre} aún no posee materias.</h2>  
    </div> 
{/if}
{include file='templates/main/footer.tpl'}

