{include file='templates/main/header.tpl'}
{include file='templates/main/navbar.tpl'}

<h1 class="titulos">{$materia->nombre}</h1>
<p class="desc"><b>Descripción:</b> {$materia->descripcion}</p>
<p class="desc"><b>Profesor:</b> {$materia->nombreProfesor}</p>
<div class="text-center">
    {foreach from=$imagenes item=$imagen}
        <div>
            <img src="{$imagen->path}" class="images-detalle">
        </div>
    {/foreach}
</div>

{if $mail}
    <div data-email="{$mail}" id="user-send"></div> 
{/if}
{if $admin}
    <div id="admin-send"></div>  
{/if}
<div data-id-materia="{$materia->id}" id="show-comentarios">
    {include file='templates/vue/comentariosList.tpl'}
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
 
<script src="js/app.js"></script>
{include file='templates/main/footer.tpl'}