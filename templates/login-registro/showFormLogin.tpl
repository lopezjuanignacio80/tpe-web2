{include file='templates/main/header.tpl'}
{include file='templates/main/navbar.tpl'}
<section class="vh-100 bg-image">
  <div class="mask d-flex align-items-center h-100 gradient-custom-3">
    <div class="container h-100">
      <div class="row d-flex justify-content-center align-items-center h-100 my-1">
        <div class="col-12 col-md-9 col-lg-7 col-xl-6">
          <div class="card" style="border-radius: 15px;">
            <div class="card-body p-5">
              <h2 class="text-center mb-5 text-dark">Ingrese a su cuenta</h2>

              <form action="login/verificar" method="post">

                <div class="form-outline mb-4">
                  {if $email}
                    <input type="email" id="form3Example3cg" class="form-control form-control-lg" name="email" value={$email} required/>
                  {else}
                    <input type="email" id="form3Example3cg" class="form-control form-control-lg" name="email" required/>
                  {/if}
                  <label class="form-label" for="form3Example3cg">Your Email</label>
                </div>

                <div class="form-outline mb-4">
                  {if $password}
                    <input type="password" id="form3Example4cg" class="form-control form-control-lg" name="password" value={$password} required/>
                  {else} 
                    <input type="password" id="form3Example4cg" class="form-control form-control-lg" name="password" required/>
                  {/if}
                  <label class="form-label" for="form3Example4cg">Password</label>
                </div>

                <div class="d-flex justify-content-center">
                  <button type="submit" class="btn btn-secondary btn-block btn-lg gradient-custom-4 text-body">Ingresar</button>
                </div>
                {if $usuarioNoExiste}
                  <div>
                    <p class="alert alert-warning">Email no registrado</p>
                  </div>
                {/if}
                {if $contraseniaInvalida}
                  <div>
                    <p class="alert alert-warning">Contraseña inválida</p>
                  </div>
                {/if}
                <p class="text-center text-muted mt-5 mb-0">Haven't already an account? <a href="registro" class="fw-bold text-body"><u>Sign up here</u></a></p>

              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
{include file='templates/main/footer.tpl'}