{include file='templates/main/header.tpl'}
{include file='templates/main/navbar.tpl'}
<section class="vh-100 bg-image">
  <div class="mask d-flex align-items-center h-100 gradient-custom-3">
    <div class="container h-100">
      <div class="row d-flex justify-content-center align-items-center h-100 my-5">
        <div class="col-12 col-md-9 col-lg-7 col-xl-6">
          <div class="card" style="border-radius: 15px;">
            <div class="card-body p-5">
              <h2 class="text-center mb-5 text-dark">Cree su cuenta GRATIS!</h2>

              <form action="registro/verificar" method="post">

                <div class="form-outline mb-4">
                  {if $email}
                    <input type="email" id="form3Example3cg" class="form-control form-control-lg" name="email" value={$email} required/>
                  {else}
                    <input type="email" id="form3Example3cg" class="form-control form-control-lg" name="email" required/>
                  {/if}
                  <label class="form-label" for="form3Example3cg">Your Email</label>
                </div>

                <div class="form-outline mb-4">
                  {if $password}
                    <input type="password" id="form3Example4cg" class="form-control form-control-lg" name="password" value={$password} required/>
                  {else} 
                    <input type="password" id="form3Example4cg" class="form-control form-control-lg" name="password" required/>
                  {/if}
                  <label class="form-label" for="form3Example4cg">Password</label>
                </div>

                <div class="form-outline mb-4">
                  {if $passwordRepeat}
                    <input type="password" id="form3Example4cdg" class="form-control form-control-lg" name="password-repeat" value={$passwordRepeat} required/>
                  {else} 
                    <input type="password" id="form3Example4cdg" class="form-control form-control-lg" name="password-repeat" required/>
                  {/if}
                  <label class="form-label" for="form3Example4cdg">Repeat your password</label>
                </div>
                <div class="form-check d-flex justify-content-center mb-5">
                    <input
                      class="form-check-input me-2"
                      type="checkbox"
                      value=""
                      id="form2Example3c"
                    />
                    <label class="form-check-label" for="form2Example3">
                      Deseo recibir novedades sobre <a href="">TUDAI</a>
                    </label>
                  </div>
                <div class="d-flex justify-content-center">
                  <button type="submit" class="btn btn-secondary btn-block btn-lg gradient-custom-4 text-body">Crear</button>
                </div>
                {if $errorNoCoinciden}
                  <div>
                    <p class="alert alert-warning">Las contraseñas no coinciden</p>
                  </div>
                {/if}
                {if $usuarioExistente}
                  <div>
                    <p class="alert alert-warning">Cuenta ya registrada</p>
                  </div>
                {/if}
                <p class="text-center text-muted mt-5 mb-0">Have already an account? <a href="login" class="fw-bold text-body"><u>Sign in here</u></a></p>

              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
{include file='templates/main/footer.tpl'}