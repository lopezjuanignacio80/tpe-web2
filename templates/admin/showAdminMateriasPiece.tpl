<h1 class="titulos">Administre materias</h1>
<div class="div-list">
    <h3>Ingrese nueva materia:</h3>
    <form action="admin/createMateria" method="post">
        <input type="text" name="nombre" id="nombre" placeholder="Materia" required>
        <input type="text" name="descripcion" id="descripcion" placeholder="Descripción" required>
        <select name="profesor" required>
        {foreach from=$profesores item=$profesor}
            <option value='{$profesor->id}'>{$profesor->nombre}</option>
        {/foreach}
        </select>
        <input type="submit" class="btn btn-secondary" value="Enviar">
    </form>
    {if $materiaRepetida}
        <p class="alert alert-warning">Materia ya cargada, por favor ingrese otra o cambie el profesor</p>
    {/if}
    <ul class="list-materias-admin">
    {foreach from=$materias item=$materia}
        <li class="li-materias">{$materia->nombre}</li><a class='btn btn-success btn-sm' href='admin/showEditMateria/{$materia->id}'>EDIT</a>
        <a class='btn btn-danger btn-delete btn-sm' id='admin/deleteMateria/{$materia->id}'>DELETE</a>
        {if $admin}
            <div>
                <div class="d-inline-flex">
                    {foreach from=$imagenes item=$imagen}
                        {if $imagen->id_materia == $materia->id}
                            <div class="image-container">
                                <div>
                                    <img src="{$imagen->path}" class="imagenes-materia">
                                </div>
                                <div class="text-center btn-delete-image">
                                    <a class='btn btn-primary btn-delete btn-sm' id='admin/deleteImg/{$imagen->path}'>delete this</a>
                                </div>
                            </div>
                        {/if}
                    {/foreach}
                </div>
            </div>
        {/if}
    {/foreach}  
    {if $admin}
        <div id="form-send-img">
            <h3>Agregue imagenes a sus materias</h3>
            <form action="admin/subirImg" method="POST" enctype="multipart/form-data">
                <input type="file" name="imagesToUpload[]" id="imagesToUpload" multiple required>
                <select name="materia" required>
                {foreach from=$materias item=$materia}
                    <option value='{$materia->id}'>{$materia->nombre}</option>
                {/foreach}
                </select>
                <button type="submit" class="btn btn-warning">SEND</button>
        …   </form>
            {if $errorCargaImagen}
                <p class="alert alert-danger">Formato de imágen no válido (JPEG - JPG - PNG available).</p>
            {/if}
        </div>
    {/if}  
    
    </ul>
</div>