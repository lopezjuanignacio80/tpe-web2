{include file='templates/main/header.tpl'}
{include file='templates/main/navbar.tpl'}
{include file='templates/admin/nav-admin.tpl'}

{include file='templates/admin/showAdminProfesoresPiece.tpl'}
<div class="div-list">
    <form action="admin/updateProfesor/{$profesorEditar->id}" method="post" id="form-edit">
        <input type="text" name="nombre" id="nombre" value="{$profesorEditar->nombre}" placeholder="Nombre y apellido" required>
        <input type="text" name="DNI" id="DNI" value="{$profesorEditar->DNI}" placeholder="DNI" required>
        <input type="submit" class="btn btn-secondary" value="Editar">
    </form>
    {if $dniRepetidoEdit}
        <p class="alert alert-warning">DNI en uso, por favor utilice el mismo DNI o uno nuevo</p>
    {/if}
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="js/btns-delete.js"></script>
<script src="js/scroll-down.js"></script>
{include file='templates/main/footer.tpl'}