<h1 class="titulos">Administre profesores</h1>
<div class="div-list">
    <h3>Ingrese nuevo profesor:</h3>
    <form action="admin/createProfesor" method="post">
        <input type="text" name="nombre" id="nombre" placeholder="Nombre y apellido" required>
        <input type="text" name="DNI" id="DNI" placeholder="DNI" required>
        <input type="submit" class="btn btn-secondary" value="Enviar">
    </form>
    {if $dniRepetido}
        <p class="alert alert-warning">DNI en uso</p>
    {/if}
    <ul>
        {foreach from=$profesores item=$profesor}
            <li class="li-materias">{$profesor->nombre}</li><a class='btn btn-success btn-sm' href='admin/showEditProfesor/{$profesor->id}'>EDIT</a>
            <a class='btn btn-danger btn-delete btn-sm' id='admin/deleteProfesor/{$profesor->id}'>DELETE</a>
            {if $profesorError && $profesor->id==$profesorError->id} 
                <p class="alert alert-danger">Usted no puede eliminar un profesor con materias asignadas!<p>
            {/if}
        {/foreach}
    </ul>
</div>