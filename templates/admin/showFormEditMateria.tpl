{include file='templates/main/header.tpl'}
{include file='templates/main/navbar.tpl'}
{include file='templates/admin/nav-admin.tpl'}

{include file='templates/admin/showAdminMateriasPiece.tpl'}
<div class="div-list">
    <form action="admin/updateMateria/{$materiaEditar->id}" method="post" id="form-edit">
        <input type="text" name="nombre" id="nombre" value="{$materiaEditar->nombre}" placeholder="Materia" required>
        <input type="text" name="descripcion" id="descripcion" value="{$materiaEditar->descripcion}" placeholder="Descripción" required>
        <select name="profesor" required>
        {foreach from=$profesores item=$profesor}
            <option value='{$profesor->id}'>{$profesor->nombre}</option>
        {/foreach}
        </select>
        <input type="submit" class="btn btn-secondary" value="Editar">
    </form>
    {if $materiaRepetidaEdit}
        <p class="alert alert-warning">Esa materia con ese profesor ya existen</p>
    {/if}
</div>
<script src="js/scroll-down.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="js/btns-delete.js"></script>
{include file='templates/main/footer.tpl'}


