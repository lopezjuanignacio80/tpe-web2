{include file='templates/main/header.tpl'}
{include file='templates/main/navbar.tpl'}

<div class="div-list">
    <h1 class="titulos">Usuarios registrados</h1>
    <ul>
        {foreach from=$usuarios item=$usuario}
            {if $usuario->email != $mail}
                <div class="d-flex">
                    <li class="li-materias">{$usuario->email}</li>
                    <form action="admin/usuarios/editarPermisos/{$usuario->email}" method="POST">
                        <div class="d-flex">
                            <div class="form-check form-switch">
                                <input class="form-check-input" name="checkbox" type="checkbox" id="flexSwitchCheckChecked" {if $usuario->admin == 1} checked {/if}>
                                <label class="form-check-label check-permiso" for="flexSwitchCheckChecked">{if $usuario->admin == 1} Administración habilitada {else} Administración deshabilitada {/if}</label>
                            </div>
                            <div>
                                <input type="submit" class="btn btn-warning btn-sm btn-send-permiso" value="ENVIAR">
                            </div>
                        </div>
                    </form>
                </div>
                <a class='btn btn-danger btn-delete btn-sm' id='admin/deleteUsuario/{$usuario->email}'>DELETE</a>
            {/if}
        {/foreach}
    </ul>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="js/btns-delete.js"></script>
{include file='templates/main/footer.tpl'}