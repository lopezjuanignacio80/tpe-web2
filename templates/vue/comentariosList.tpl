{literal}

<div id="app">
    <div class="d-flex justify-content-center" v-if="user"> 
        <div class="dropdown">
            <button class="btn btn-danger dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Sort by
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                <button class="dropdown-item" type="button" id="fechabtn" v-on:click="sortByDate">Antiguedad</button>
                <button class="dropdown-item" type="button" id="puntajebtn" v-on:click="sortByPuntaje">Puntaje</button>
            </div>
        </div>
        <div class="dropdown">
            <button class="btn btn-danger dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Order by
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                <button class="dropdown-item" type="button" id="ascbtn" v-on:click="orderByAsc">Asc</button>
                <button class="dropdown-item" type="button" id="descbtn" v-on:click="orderByDesc">Desc</button>
            </div>
        </div>
        <div class="dropdown">
            <button class="btn btn-danger dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Puntaje
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                <button class="dropdown-item puntaje-drop" type="button" value="0" v-on:click="filtarPuntaje(0)">All</button>
                <button class="dropdown-item puntaje-drop" type="button" value="1" v-on:click="filtarPuntaje(1)">1</button>
                <button class="dropdown-item puntaje-drop" type="button" value="2" v-on:click="filtarPuntaje(2)">2</button>
                <button class="dropdown-item puntaje-drop" type="button" value="3" v-on:click="filtarPuntaje(3)">3</button>
                <button class="dropdown-item puntaje-drop" type="button" value="4" v-on:click="filtarPuntaje(4)">4</button>
                <button class="dropdown-item puntaje-drop" type="button" value="5" v-on:click="filtarPuntaje(5)">5</button>
            </div>
        </div>
    </div>
    

    <div v-for="comentario in comentarios" class="div-for-comentario">
        <p class="comment-user"><span class="comment-by">Comment by </span>{{comentario.mail_usuario}}</p>
        <p class="comment-user">
            {{comentario.fecha}} <span v-for= "index in parseInt(comentario.puntaje)" class="fa fa-star fa-checked"></span>    
        </p>
        <p class="comment-content">{{comentario.descripcion}} </p> 
        <div class="text-center">
            <button v-if="admin" class="btn btn-danger btn-delete btn-sm btn-delete-comentario" type="submit" v-on:click="eliminarComentario(comentario.id)">DELETE</button>
        </div>  
    </div>

    <form id="form-send-comment" method="POST" v-if="user" v-on:submit.prevent="submitComentario">
        <textarea name="txtarea" cols="69" rows="7" class="div-for-comentario" placeholder="Ingrese comentario.." required></textarea>
        <div id="div-estrellas">
            <p class="clasificacion">
            <input id="radio1" type="radio" name="estrellas" value="5" >
            <label for="radio1">★</label>
            <input id="radio2" type="radio" name="estrellas" value="4">
            <label for="radio2">★</label>
            <input id="radio3" type="radio" name="estrellas" value="3">
            <label for="radio3">★</label>
            <input id="radio4" type="radio" name="estrellas" value="2">
            <label for="radio4">★</label>
            <input id="radio5" type="radio" name="estrellas" value="1" checked >
            <label for="radio5">★</label>
            </p>
        </div>
        <div class="d-flex justify-content-center">
            <button type="submit" class="btn btn-outline-info">SEND</button>
        </div>
    </form>
</div>

{/literal}