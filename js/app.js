"use strict"

const API_URL = "api/comentarios";


let div = document.querySelector("#show-comentarios");
let idMateria = div.dataset.idMateria;

async function postComentario(nuevoComentario) {
    try {
        let res = await fetch(API_URL, {
            "method": "POST",
            "headers": { "Content-type": "application/json" },
            "body": JSON.stringify(nuevoComentario),
        });
        if (res.status == 200) {
            console.log("Creado!");
            getComentarios(urlOrdenamientoFiltrado+sort+order+puntaje);
            let formComentario = document.querySelector("#form-send-comment");
            formComentario.reset();
        }
    } catch (error) {
        console.log(error);
    }
}
function generarJson(idMateria, email, descripcion, puntaje, fecha) {
    let nuevoComentario = {
        "id_materia": idMateria,
        "mail_usuario": email,
        "descripcion": descripcion,
        "puntaje": puntaje,
        "fecha": fecha,
    }
    return nuevoComentario;
}
async function agregarComentario() {
    let formComentario = document.querySelector("#form-send-comment");
    let formData = new FormData(formComentario);
    let puntaje = formData.get("estrellas");
    puntaje = Number(puntaje);
    let descripcion = formData.get("txtarea");
    let email_div = document.querySelector("#user-send");
    let email = email_div.dataset.email;
    idMateria = Number(idMateria);
    let fecha = new Date().toISOString().slice(0, 19).replace('T', ' ');
    let nuevoComentario = generarJson(idMateria, email, descripcion, puntaje, fecha);
    postComentario(nuevoComentario);
}


let app = new Vue({
    el: "#app",
    data: {
        comentarios: [],
        admin: false,
        user:false
    },
    methods: {
        eliminarComentario: function(itemId) {deleteComentario(itemId)},
        sortByDate: function(){sortByDate()},
        sortByPuntaje: function(){sortByPuntaje()},
        orderByAsc: function(){orderByAsc()},
        orderByDesc:function(){orderByDesc()},
        filtarPuntaje: function(value){filtarPuntaje(value)},
        submitComentario: function(){agregarComentario()},
    }
});

let admin = document.querySelector("#admin-send");
if (admin != null) {
    app.admin = true;
}
let user = document.querySelector("#user-send");
if (user != null) {
    app.user = true;
}

async function deleteComentario(idComentario) {
    try {
        let res = await fetch(API_URL + "/" + idComentario, {
            "method": "DELETE",
        });
        if (res.status == 200) {
            console.log("Borrado!");
            getComentarios(urlOrdenamientoFiltrado+sort+order+puntaje);
        }
    } catch (e) {
        console.log(e);
    }
}

async function getComentarios(url) {
    try {
        let response = await fetch(url);
        let comentarios = await response.json();
        app.comentarios = comentarios;
    } catch (e) {
        console.log(e);
    }
}

function getComentariosMateria() {
    let url = API_URL + "?id_materia=" + idMateria;
    getComentarios(url);
}


getComentariosMateria();

let sort="";
let order="";
let puntaje="";
let urlOrdenamientoFiltrado= API_URL+"?id_materia="+idMateria;
function sortByDate() {
    sort="&sort=fecha";
    getComentarios(urlOrdenamientoFiltrado+sort+order+puntaje); 
}
function sortByPuntaje() {
    sort="&sort=puntaje";
    getComentarios(urlOrdenamientoFiltrado+sort+order+puntaje); 
}
function orderByAsc(){
    order="&order=asc"; 
    getComentarios(urlOrdenamientoFiltrado+sort+order+puntaje); 
}
function orderByDesc(){
    order="&order=desc";
    getComentarios(urlOrdenamientoFiltrado+sort+order+puntaje);   
}


function filtarPuntaje(value){
    let numeroPuntaje=value;
    if (numeroPuntaje!="0"){ //si es cero, es ALL puntajes
        puntaje= "&puntaje="+numeroPuntaje;
    }else{
        puntaje="";
    }
    getComentarios(urlOrdenamientoFiltrado+sort+order+puntaje);
}




