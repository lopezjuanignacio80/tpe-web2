let btnsDelete = document.querySelectorAll(".btn-delete");
btnsDelete.forEach((element) => {
  element.addEventListener("click", () => {
    swal({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      buttons: {
        cancel: true,
        confirm: true,
      },
    }).then((result) => {
        if (result) {
            window.location.href =element.id;
        };
    });   
  });
});
