if (document.querySelector("#input-filtro-nombre")!=null){
    document.querySelector("#input-filtro-nombre").addEventListener("keyup",filtrarNombre);
}
if (document.querySelector("#input-filtro-descripcion")!=null){
    document.querySelector("#input-filtro-descripcion").addEventListener("keyup",filtrarDescripcion);
}
let filtro="";

//se debe guardar el valor anterior de los inputs, por si se desea paginar, para no perderlos
if (document.querySelector("#input-filtro-nombre")!=null && document.querySelector("#input-filtro-descripcion")!=null){
    if (document.querySelector("#input-filtro-descripcion").value == ""){
        filtro="home?filtrarNombre=" + document.querySelector("#input-filtro-nombre").value;
    }
    else{
        filtro="home?filtrarDescripcion=" +document.querySelector("#input-filtro-descripcion").value;
    }
}

let paginado="";

function filtrarNombre(){
    let guardar=document.querySelector("#input-filtro-nombre").value; 
    filtro = `home?filtrarNombre=${guardar}`;
    goThere(filtro);
};
function filtrarDescripcion(){
    let guardar=document.querySelector("#input-filtro-descripcion").value; 
    filtro = `home?filtrarDescripcion=${guardar}`;
    goThere(filtro);
};

let links_paginado= document.querySelectorAll(".page-link");
links_paginado.forEach(element => {
    element.addEventListener("click",function() {    
        paginado= "&nroPag=" + element.dataset.paginado;
        goThere(filtro+paginado);
    })
});

function goThere(url){
    window.location.href=url;
}