<?php

require_once 'libs/Router.php';
require_once 'Controller/ApiComentarioController.php';

// crea el router
$router = new Router();

// define la tabla de ruteo
$router->addRoute('comentarios','GET', 'ApiComentarioController', 'getComentarios');
$router->addRoute('comentarios/:ID', 'GET', 'ApiComentarioController', 'getComentario');
$router->addRoute('comentarios/:ID', 'DELETE', 'ApiComentarioController', 'deleteComentario');
$router->addRoute('comentarios', 'POST', 'ApiComentarioController', 'addComentario');
$router->addRoute('comentarios/:ID','PUT', 'ApiComentarioController', 'updateComentario');
// rutea
$router->route($_GET["resource"], $_SERVER['REQUEST_METHOD']);