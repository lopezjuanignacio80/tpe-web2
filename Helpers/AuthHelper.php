<?php

class AuthHelper
{

    function __construct()
    {
    }

    function checkNoRegistrado()
    {
        session_start();
        if (!isset($_SESSION["email"])) {
            header("Location: " . BASE_URL . "home");
            die;
        }
    }

    function checkRegistrado()
    {
        session_start();
        if (isset($_SESSION["email"])) {
            header("Location: " . BASE_URL . "home");
            die;
            //al ser admin o usuario registrado no puedo ver los formularios (registro y login)
        }
    }

    function checkAdmin()
    {
        session_start();
        if (!isset($_SESSION["admin"])) {
            header("Location: " . BASE_URL . "home");
            die;
        }
    }
    function logout()
    {
        session_start();
        session_destroy();
        header("Location: " . BASE_URL . "home");
        die;
    }
}
