<?php

require_once "libs/smarty/libs/Smarty.class.php";

class UsuarioView
{
    private $smarty;

    function __construct()
    {
        $this->smarty = new Smarty();
    }

    function showFormLogin($usuarioNoExiste = null, $contraseniaInvalida = null, $email = null, $password = null)
    {
        $this->smarty->assign('mail', null);
        $this->smarty->assign('usuarioLogueado', null);
        $this->smarty->assign('usuarioNoExiste', $usuarioNoExiste);
        $this->smarty->assign('contraseniaInvalida', $contraseniaInvalida);
        $this->smarty->assign('email', $email);
        $this->smarty->assign('admin', null);
        $this->smarty->assign('password', $password);
        $this->smarty->display('templates/login-registro/showFormLogin.tpl');
    }

    function showFormRegistro($errorNoCoinciden = null, $email = null, $password = null, $passwordRepeat = null, $usuarioExistente = null)
    {
        $this->smarty->assign('mail', null);
        $this->smarty->assign('usuarioLogueado', null);
        $this->smarty->assign('usuarioExistente', $usuarioExistente);
        $this->smarty->assign('email', $email);
        $this->smarty->assign('password', $password);
        $this->smarty->assign('passwordRepeat', $passwordRepeat);
        $this->smarty->assign('admin', null);
        $this->smarty->assign('errorNoCoinciden', $errorNoCoinciden);
        $this->smarty->display('templates/login-registro/showFormRegistro.tpl');
    }

    function showSuccessRegistro()
    {
        $this->smarty->assign('mail', $_SESSION["email"]);
        $this->smarty->assign('usuarioLogueado', true);
        $this->smarty->assign('admin', null);
        $this->smarty->display('templates/login-registro/showSuccessRegistro.tpl');
    }
    function showHomeLocation()
    {
        header("Location: " . BASE_URL . "home");
    }
    function showUsuarios($usuarios)
    {
        $this->smarty->assign('mail', $_SESSION['email']);
        $this->smarty->assign('usuarios', $usuarios);
        $this->smarty->assign('usuarioLogueado', true);
        $this->smarty->assign('admin', true);
        $this->smarty->display('templates/admin/showAdminUsuarios.tpl');
    }
    function showAdminUsuariosLocation()
    {
        header("Location: " . BASE_URL . "admin/usuarios");
    }
    function showUsuarioFail()
    {
        $this->smarty->assign('mail', $_SESSION['email']);
        $this->smarty->assign('admin', true);
        $this->smarty->assign('usuarioLogueado', true);
        $this->smarty->display('templates/errors/showUsuarioFail.tpl');
    }
}
