<?php

require_once "libs/smarty/libs/Smarty.class.php";

class MateriaView
{

    private $smarty;

    function __construct()
    {
        $this->smarty = new Smarty();
    }

    function showMaterias($materias, $valorInputDescripcion = null, $valorInputNombre = null, $cantPags = null, $pagActual = null, $usuarioLogueado = null, $admin = null)
    {
        $this->smarty->assign('materias', $materias);
        if ($usuarioLogueado) {
            $this->smarty->assign('mail', $_SESSION["email"]);
        }
        $this->smarty->assign('valorInputNombre', $valorInputNombre);
        $this->smarty->assign('valorInputDescripcion', $valorInputDescripcion);
        $this->smarty->assign('pagActual', $pagActual);
        $this->smarty->assign('cantPags', $cantPags);
        $this->smarty->assign('usuarioLogueado', $usuarioLogueado);
        $this->smarty->assign('admin', $admin);
        $this->smarty->display('templates/main/home.tpl');
    }

    function showAdminLocation()
    {
        header("Location: " . BASE_URL . "admin");
    }

    function showMateria($imagenes, $materia, $usuarioLogueado = null, $admin = null)
    {
        if ($usuarioLogueado) {
            $this->smarty->assign('mail', $_SESSION["email"]);
        } else {
            $this->smarty->assign('mail', null);
        }
        $this->smarty->assign('imagenes', $imagenes);
        $this->smarty->assign('materia', $materia);
        $this->smarty->assign('usuarioLogueado', $usuarioLogueado);
        $this->smarty->assign('admin', $admin);
        $this->smarty->display('templates/public-user/showDetalleMateria.tpl');
    }

    function showMateriaFail($usuarioLogueado = null, $admin = null)
    {
        if ($usuarioLogueado) {
            $this->smarty->assign('mail', $_SESSION["email"]);
        }
        $this->smarty->assign('usuarioLogueado', $usuarioLogueado);
        $this->smarty->assign('admin', $admin);
        $this->smarty->display('templates/errors/showMateriaFail.tpl');
    }

    function showMateriasProfe($materias, $profesor, $usuarioLogueado = null, $admin = null)
    {
        if ($usuarioLogueado) {
            $this->smarty->assign('mail', $_SESSION["email"]);
        }
        $this->smarty->assign('materias', $materias);
        $this->smarty->assign('usuarioLogueado', $usuarioLogueado);
        $this->smarty->assign('profesor', $profesor);
        $this->smarty->assign('admin', $admin);
        $this->smarty->display('templates/public-user/showMateriasProfe.tpl');
    }

    function showAdminMaterias($imagenes = null, $materias, $profesores, $materiaRepetida = null, $admin = null, $errorCargaImagen = null)
    {
        $this->smarty->assign('mail', $_SESSION["email"]);
        $this->smarty->assign('usuarioLogueado', true);
        $this->smarty->assign('imagenes', $imagenes);
        $this->smarty->assign('materias', $materias);
        $this->smarty->assign('profesores', $profesores);
        $this->smarty->assign('admin', $admin);
        $this->smarty->assign('errorCargaImagen', $errorCargaImagen);
        $this->smarty->assign('materiaRepetida', $materiaRepetida);
        $this->smarty->display('templates/admin/showAdminMaterias.tpl');
    }

    function showFormEdit($imagenes = null, $materias, $profesores, $materiaEditar, $materiaRepetidaEdit = null, $admin = null)
    {
        $this->smarty->assign('mail', $_SESSION["email"]);
        $this->smarty->assign('materiaRepetida', null);
        $this->smarty->assign('usuarioLogueado', true);
        $this->smarty->assign('imagenes', $imagenes);
        $this->smarty->assign('materias', $materias);
        $this->smarty->assign('profesores', $profesores);
        $this->smarty->assign('errorCargaImagen', null);
        $this->smarty->assign('materiaEditar', $materiaEditar);
        $this->smarty->assign('admin', $admin);
        $this->smarty->assign('materiaRepetidaEdit', $materiaRepetidaEdit);
        $this->smarty->display('templates/admin/showFormEditMateria.tpl');
    }

    function showMateriasFiltradas($materias)
    {
        $this->smarty->assign('materias', $materias);
        $this->smarty->display('templates/public-user/showMateriasFiltradas.tpl');
    }

    function show404()
    {
        $this->smarty->display('templates/errors/error404.tpl');
    }
}
