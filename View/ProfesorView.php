<?php

require_once "libs/smarty/libs/Smarty.class.php";

class ProfesorView
{

    private $smarty;

    function __construct()
    {
        $this->smarty = new Smarty();
    }

    function showProfesorFail($usuarioLogueado = null,$admin=null)
    {
        if ($usuarioLogueado) {
            $this->smarty->assign('mail', $_SESSION["email"]);
        }
        $this->smarty->assign('admin', $admin);
        $this->smarty->assign('usuarioLogueado', $usuarioLogueado);
        $this->smarty->display('templates/errors/showProfesorFail.tpl');
    }

    function showProfesores($profesores, $usuarioLogueado = null,$admin=null)
    {
        if ($usuarioLogueado) {
            $this->smarty->assign('mail', $_SESSION["email"]);
        }
        $this->smarty->assign('usuarioLogueado', $usuarioLogueado);
        $this->smarty->assign('profesores', $profesores);
        $this->smarty->assign('admin', $admin);
        $this->smarty->display('templates/public-user/showProfesores.tpl');
    }

    function showAdminLocation()
    {
        header("Location: " . BASE_URL . "admin/profesores");
    }

    function showAdminProfesores($profesores, $profesorError = null, $dniRepetido = null,$admin=null)
    {
        $this->smarty->assign('mail', $_SESSION["email"]);
        $this->smarty->assign('usuarioLogueado', true);
        $this->smarty->assign('profesores', $profesores);
        $this->smarty->assign('admin', $admin);
        $this->smarty->assign('profesorError', $profesorError);
        $this->smarty->assign('dniRepetido', $dniRepetido);
        $this->smarty->display('templates/admin/showAdminProfesores.tpl');
    }

    function showFormEdit($profesores, $profesorEditar, $dniRepetidoEdit = null,$admin=null)
    {
        $this->smarty->assign('mail', $_SESSION["email"]);
        $this->smarty->assign('dniRepetido', null);
        $this->smarty->assign('profesorError', null);
        $this->smarty->assign('usuarioLogueado', true);
        $this->smarty->assign('admin', $admin);
        $this->smarty->assign('profesores', $profesores);
        $this->smarty->assign('profesorEditar', $profesorEditar);
        $this->smarty->assign('dniRepetidoEdit', $dniRepetidoEdit);
        $this->smarty->display('templates/admin/showFormEditProfesor.tpl');
    }
}
